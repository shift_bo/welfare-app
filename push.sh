#!/bin/sh
yarn build
zip -r calendar-app.zip dist/*
scp calendar-app.zip notice:/data/web_root
ssh notice < pull.sh