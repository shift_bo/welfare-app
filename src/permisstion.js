import router from '@/router'
import { getCookie } from '@/utils/cookie'
import store from './store'

router.beforeEach(async (to, from, next) => {
  const token = getCookie()
  if (token) {
    let user = store.getters.user
    if (JSON.stringify(user) === '{}') {
      user = await store.dispatch('GetUserInfo')
      next()
    }
    next()
  } else {
    if (process.env.NODE_ENV === 'production') {
      window.location.href = process.env.VUE_APP_BASE_URL + '/v/weixin/oauth2/web'
    } else {
      let user = store.getters.user
      if (JSON.stringify(user) === '{}') {
        user = await store.dispatch('GetUserInfo')
        next()
      }
      next()
    }
  }
})

export default router
